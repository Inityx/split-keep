#![feature(pattern)]
#![deny(clippy::all)]

use std::{
    iter::Peekable,
    str::{MatchIndices, pattern::Pattern},
};

pub struct SplitKeep<'src, P: Pattern<'src>> {
    source: &'src str,
    match_indices: Peekable<MatchIndices<'src, P>>,
    cursor: usize,
}

impl<'src, P: Pattern<'src>> SplitKeep<'src, P> {
    pub fn of(source: &'src str, pattern: P) -> Self {
        SplitKeep {
            source,
            match_indices: source.match_indices(pattern).peekable(),
            cursor: 0
        }
    }

    fn end(&self) -> usize {
        self.source.len()
    }

    fn upcoming_separator(&mut self) -> Option<usize> {
        self.match_indices.peek().map(|&(start, _)| start)
    }

    fn up_to(&self, index: usize) -> (usize, &'src str) {
        (index, &self.source[self.cursor..index])
    }

    fn consume_separator(&mut self) -> (usize, &'src str) {
        match self.match_indices.next() {
            Some((index, token)) => (index + token.len(), token),
            None => unreachable!(),
        }
    }
}

impl<'src, P: Pattern<'src>> Iterator for SplitKeep<'src, P> {
    type Item = &'src str;

    fn next(&mut self) -> Option<Self::Item> {
        let (new_cursor, next_token) = match self.upcoming_separator() {
            Some(index) if self.cursor <  index => self.up_to(index),
            Some(index) if self.cursor == index => self.consume_separator(),
            None if self.cursor <  self.end() => self.up_to(self.end()),
            None if self.cursor == self.end() => return None,
            _ => unreachable!(),
        };

        self.cursor = new_cursor;
        Some(next_token)
    }
}

pub trait SplitKeepExt {
    fn split_keep<'src, P: Pattern<'src>>(&'src self, pat: P) -> SplitKeep<'src, P>;
}

impl SplitKeepExt for str {
    fn split_keep<'src, P: Pattern<'src>>(&'src self, pat: P) -> SplitKeep<'src, P> {
        SplitKeep::of(self, pat)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use regex::Regex;

    #[test]
    fn orientation() {
        assert_eq!(
            &["a", ".", "b", ".", "c"],
            "a.b.c".split_keep('.').collect::<Vec<_>>().as_slice(),
        );
        assert_eq!(
            &[".", "a", ".", "b", ".", "c", "."],
            ".a.b.c.".split_keep('.').collect::<Vec<_>>().as_slice(),
        );
        assert_eq!(
            &[".", "a", ".", "b", ".", "c"],
            ".a.b.c".split_keep('.').collect::<Vec<_>>().as_slice(),
        );
        assert_eq!(
            &["a", ".", "b", ".", "c", "."],
            "a.b.c.".split_keep('.').collect::<Vec<_>>().as_slice(),
        );
    }

    #[test]
    fn multiple() {
        assert_eq!(
            &["foo ", "(", "thing", "(", "thang", ")", ")"],
            "foo (thing(thang))"
                .split_keep(|c| "()".contains(c))
                .collect::<Vec<_>>()
                .as_slice(),
        )
    }

    #[test]
    fn long() {
        assert_eq!(
            &["a", "<->", "b", "<->", "c"],
            "a<->b<->c".split_keep("<->").collect::<Vec<_>>().as_slice(),
        )
    }

    #[test]
    fn variable() {
        let pattern = Regex::new("<[a-z]+>").unwrap();
        assert_eq!(
            &["a", "<abc>", "b", "<defg>", "c"],
            "a<abc>b<defg>c".split_keep(&pattern).collect::<Vec<_>>().as_slice(),
        )
    }
}
